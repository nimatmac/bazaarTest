package com.nima.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.nima.database.dao.CategoryDao
import com.nima.database.dao.PhotoDao
import com.nima.database.dao.TipDao
import com.nima.database.dao.VenueDao
import com.nima.models.remote.entity.CategoryEntity
import com.nima.models.remote.entity.PhotoEntity
import com.nima.models.remote.entity.TipEntity
import com.nima.models.remote.entity.VenueEntity
import com.nima.utils.ContextUtils

@Database(entities = [VenueEntity::class, CategoryEntity::class, PhotoEntity::class, TipEntity::class], version = 1)
abstract class FoursquareDatabase : RoomDatabase() {
    abstract fun venueDao(): VenueDao
    abstract fun categoryDao(): CategoryDao
    abstract fun photoDao(): PhotoDao
    abstract fun tipDao(): TipDao

    companion object {
        private var INSTANCE: FoursquareDatabase? = null
        fun getDatabase(): FoursquareDatabase {
            if (INSTANCE == null) {
                synchronized(FoursquareDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        ContextUtils.applicationContext,
                        FoursquareDatabase::class.java,
                        "fourSquare.db"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE!!
        }
    }
}