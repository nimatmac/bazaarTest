package com.nima.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nima.models.remote.PHOTO_TABLE_NAME
import com.nima.models.remote.entity.PhotoEntity

@Dao
interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(photoEntities: List<PhotoEntity>)

    @Query("DELETE FROM $PHOTO_TABLE_NAME")
    fun deleteAll()

    @Query("SELECT * FROM $PHOTO_TABLE_NAME WHERE ${PHOTO_TABLE_NAME}_venueId = :venueId")
    fun getPhotosOfVenue(venueId: String): LiveData<List<PhotoEntity>>

    @Query("SELECT COUNT(${PHOTO_TABLE_NAME}_id) FROM $PHOTO_TABLE_NAME WHERE ${PHOTO_TABLE_NAME}_venueId = :venueId")
    suspend fun getVenuePhotoCount(venueId: String): Int
}
