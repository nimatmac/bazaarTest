package com.nima.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nima.models.remote.TIP_TABLE_NAME
import com.nima.models.remote.entity.TipEntity

@Dao
interface TipDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(tipEntities: List<TipEntity>)

    @Query("DELETE FROM $TIP_TABLE_NAME")
    fun deleteAll()

    @Query("SELECT * FROM $TIP_TABLE_NAME WHERE ${TIP_TABLE_NAME}_venueId = :venueId")
    fun getTipsOfVenue(venueId: String): LiveData<List<TipEntity>>

}
