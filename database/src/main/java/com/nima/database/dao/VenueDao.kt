package com.nima.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nima.models.remote.CATEGORY_TABLE_NAME
import com.nima.models.remote.PHOTO_TABLE_NAME
import com.nima.models.remote.VENUE_TABLE_NAME
import com.nima.models.remote.entity.VenueEntity
import com.nima.models.remote.vo.VenueViewObject

@Dao
interface VenueDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(venues: List<VenueEntity>)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(venues: VenueEntity)
    @Query("DELETE FROM $VENUE_TABLE_NAME")
    suspend fun deleteAll()
    @Query("SELECT * FROM $VENUE_TABLE_NAME LEFT JOIN $CATEGORY_TABLE_NAME ON (${VENUE_TABLE_NAME}_catId = $CATEGORY_TABLE_NAME.${CATEGORY_TABLE_NAME}_categoryId) LEFT JOIN $PHOTO_TABLE_NAME ON (${VENUE_TABLE_NAME}_venueId = $PHOTO_TABLE_NAME.${PHOTO_TABLE_NAME}_venueId) WHERE ${VENUE_TABLE_NAME}_venueId = :venueId")
    fun getVenueLive(venueId: String?): LiveData<VenueViewObject>
    @Query("SELECT * FROM $VENUE_TABLE_NAME WHERE ${VENUE_TABLE_NAME}_venueId = :venueId")
    suspend fun getVenue(venueId: String?): VenueEntity?
    @Query("SELECT * FROM $VENUE_TABLE_NAME LEFT JOIN $CATEGORY_TABLE_NAME ON (${VENUE_TABLE_NAME}_catId = $CATEGORY_TABLE_NAME.${CATEGORY_TABLE_NAME}_categoryId) ORDER BY ${VENUE_TABLE_NAME}_distance")
    fun getVenues(): LiveData<List<VenueViewObject>>
}
