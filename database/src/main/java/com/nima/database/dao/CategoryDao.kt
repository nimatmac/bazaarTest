package com.nima.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nima.models.remote.CATEGORY_TABLE_NAME
import com.nima.models.remote.entity.CategoryEntity

@Dao
interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(categories: List<CategoryEntity>)

    @Query("SELECT * FROM category WHERE ${CATEGORY_TABLE_NAME}_categoryId = :categoryId")
    fun getCategory(categoryId: String) : CategoryEntity

    @Query("DELETE FROM category")
    fun deleteAll()
}
