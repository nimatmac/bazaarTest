package com.nima.utils

import android.content.Context
import com.nima.utils.ContextUtils.applicationContext

object PrefUtils {
    private val mSharedPreferences = applicationContext.getSharedPreferences(
        PREF_NAME,
        Context.MODE_PRIVATE
    )


    val lastLocation: Pair<Double, Double>
        get() = Pair(
            mSharedPreferences.getFloat(
                KEY_LAST_LOCATION_LATITUDE,
                -10000f
            ).toDouble(),
            mSharedPreferences.getFloat(
                KEY_LAST_LOCATION_LONGITUDE,
                -10000f
            ).toDouble()
        )

    val lastUpdateTime: Long
        get() =
            mSharedPreferences.getLong(
                KEY_LAST_UPDATE_TIME,
                0
            )

    fun saveLastLocation(latitude: Double?, longitude: Double?) {
        mSharedPreferences.edit().putFloat(
            KEY_LAST_LOCATION_LATITUDE,
            latitude?.toFloat() ?: return
        ).putFloat(
            KEY_LAST_LOCATION_LONGITUDE,
            longitude?.toFloat() ?: return
        ).apply()
    }

    fun saveLastUpdateTime(lastUpdateTime: Long) {
        mSharedPreferences.edit().putLong(
            KEY_LAST_UPDATE_TIME,
            lastUpdateTime
        ).apply()
    }
}