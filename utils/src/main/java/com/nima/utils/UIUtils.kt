package com.nima.utils

object UIUtils {
    const val THUMBNAIL_FACTOR = 0.1f

    fun pxFromDp(dp: Float): Float {
        return dp * ContextUtils.applicationContext.resources.displayMetrics.density
    }
}