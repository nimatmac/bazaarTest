package com.nima.utils

import kotlin.math.*

object LocationUtils {
    fun distanceBetween(latA: Double, lngA: Double, latB: Double, lngB: Double): Float {
        val r = 6371
        val latDistance = Math.toRadians(latB - latA)
        val lonDistance = Math.toRadians(lngB - lngA)
        val a =
            (sin(latDistance / 2) * sin(latDistance / 2)
                    + (cos(Math.toRadians(latA)) * cos(
                Math.toRadians(
                    latB
                )
            )
                    * sin(lonDistance / 2) * sin(lonDistance / 2)))
        val c = 2 * atan2(sqrt(a), sqrt(1 - a))
        var distance = r * c * 1000


        distance = distance.pow(2.0)

        return sqrt(distance).toFloat()
    }
}