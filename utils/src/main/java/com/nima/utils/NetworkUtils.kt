package com.nima.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import com.nima.utils.ContextUtils.applicationContext

object NetworkUtils {
    fun isConnected(): Boolean {
        var isConnected = false
        val connectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT < 23) {
            val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo

            if (activeNetwork != null) {
                isConnected =
                    (activeNetwork.isConnected && (activeNetwork.type == ConnectivityManager.TYPE_WIFI || activeNetwork.type == ConnectivityManager.TYPE_MOBILE))
            }
        } else {
            val activeNetwork = connectivityManager.activeNetwork

            if (activeNetwork != null) {
                val networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)

                isConnected =
                    (networkCapabilities?.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ?: false ||
                            networkCapabilities?.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ?: false)
            }
        }
        return isConnected
    }
}