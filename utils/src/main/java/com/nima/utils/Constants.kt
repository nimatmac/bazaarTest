package com.nima.utils

const val PREF_NAME = "com.nima.foursquare.PREF_NAME"
const val KEY_LAST_LOCATION_LATITUDE = "com.nima.foursquare.KEY_LAST_LOCATION_LATITUDE"
const val KEY_LAST_LOCATION_LONGITUDE = "com.nima.foursquare.KEY_LAST_LOCATION_LONGITUDE"
const val KEY_LAST_UPDATE_TIME = "com.nima.foursquare.KEY_LAST_UPDATE_TIME"
const val UPDATE_TIME_THRESHOLD = 60 * 60 * 2 * 1000L
const val NETWORK_PAGE_SIZE = 10
const val MAX_DISTANCE_TO_IGNORE_UPDATES = 100 // in meters