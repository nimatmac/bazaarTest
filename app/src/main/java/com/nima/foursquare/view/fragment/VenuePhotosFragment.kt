package com.nima.foursquare.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.nima.foursquare.R
import com.nima.foursquare.view.adapter.VenuePhotosAdapter
import com.nima.foursquare.view.viewmodel.VenuePhotosViewModel
import kotlinx.android.synthetic.main.venue_photos_fragment.*

class VenuePhotosFragment : Fragment() {

    private lateinit var viewModel: VenuePhotosViewModel
    private lateinit var adapter: VenuePhotosAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.venue_photos_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        bindViewModel()
    }

    private fun setupRecycler() {
        adapter = VenuePhotosAdapter()
        recyclerView.adapter = adapter
    }

    private fun bindViewModel() {
        val venueId = arguments?.getString(KEY_VENUE_ID)!!

        viewModel = ViewModelProvider(this).get(VenuePhotosViewModel::class.java)

        viewModel.getVenuePhotos(venueId).observe(viewLifecycleOwner, adapter::submitList)
    }

    companion object {

        private const val KEY_VENUE_ID = "KEY_VENUE_ID"

        fun newInstance(venueId: String): VenuePhotosFragment {
            val fragment = VenuePhotosFragment()
            val args = Bundle()
            args.putString(KEY_VENUE_ID, venueId)
            fragment.arguments = args
            return fragment
        }
    }
}
