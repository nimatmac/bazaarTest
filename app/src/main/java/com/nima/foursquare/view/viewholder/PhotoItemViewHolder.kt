package com.nima.foursquare.view.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.nima.foursquare.databinding.VenuePhotoListItemBinding
import com.nima.models.remote.entity.PhotoEntity

class PhotoItemViewHolder(private val itemViewBinding: VenuePhotoListItemBinding) : RecyclerView.ViewHolder(itemViewBinding.root) {

    fun bind(item: PhotoEntity?) {
        itemViewBinding.photo = item
        itemViewBinding.callback = this
    }

    fun showOriginalImage(view: View, photoOriginalUrl: String) {

    }
}