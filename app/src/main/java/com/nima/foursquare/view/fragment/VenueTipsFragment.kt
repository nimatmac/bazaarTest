package com.nima.foursquare.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.nima.foursquare.R
import com.nima.foursquare.view.adapter.VenueTipsAdapter
import com.nima.foursquare.view.viewmodel.VenueTipsViewModel
import kotlinx.android.synthetic.main.venue_tips_fragment.*

class VenueTipsFragment : Fragment() {

    private lateinit var viewModel: VenueTipsViewModel
    private lateinit var adapter: VenueTipsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.venue_tips_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        bindViewModel()
    }

    private fun setupRecycler() {
        adapter = VenueTipsAdapter()
        recyclerView.adapter = adapter
    }

    private fun bindViewModel() {
        val venueId = arguments?.getString(KEY_VENUE_ID)!!

        viewModel = ViewModelProvider(this).get(VenueTipsViewModel::class.java)
        viewModel.getVenueTips(venueId).observe(viewLifecycleOwner, adapter::submitList)
    }

    companion object {

        private const val KEY_VENUE_ID = "KEY_VENUE_ID"

        fun newInstance(venueId: String): VenueTipsFragment {
            val fragment = VenueTipsFragment()
            val args = Bundle()
            args.putString(KEY_VENUE_ID, venueId)
            fragment.arguments = args
            return fragment
        }
    }

}
