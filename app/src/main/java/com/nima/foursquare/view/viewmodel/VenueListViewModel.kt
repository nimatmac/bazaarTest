package com.nima.foursquare.view.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.nima.models.remote.vo.VenueViewObject
import com.nima.repository.Repository
import com.nima.utils.LocationUtils
import com.nima.utils.MAX_DISTANCE_TO_IGNORE_UPDATES
import com.nima.utils.PrefUtils
import com.nima.utils.UPDATE_TIME_THRESHOLD
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.CoroutineContext

class VenueListViewModel : ViewModel(), CoroutineScope {

    var isLoading = false
    private val repo = Repository.getInstance()
    private val queryLiveData = MutableLiveData<String?>()
    val recommendedVenues: LiveData<List<VenueViewObject>> =
        Transformations.switchMap(queryLiveData) {
            repo.getRecommendedVenues().also {
                loadMore(0)
            }
        }
    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job + CoroutineExceptionHandler { _, t ->
            t.printStackTrace()
            isLoading = false
        }


    fun getRecommendedVenues(latitude: Double? = null, longitude: Double? = null) = launch {
        val query: String? = if (locationIsChanged(latitude, longitude) || dataIsObsolete()) {
            Log.d(
                "tag",
                "location is Changed? ${locationIsChanged(
                    latitude,
                    longitude
                )} data is Obsolete ? ${dataIsObsolete()}"
            )
            repo.deleteAll()
            if (latitude == null || longitude == null)
                null
            else {
                PrefUtils.saveLastLocation(latitude, longitude)
                PrefUtils.saveLastUpdateTime(Calendar.getInstance().timeInMillis)
                "$latitude,$longitude"
            }
        } else {
            null
        }
        queryLiveData.postValue(query)
    }

    private fun dataIsObsolete(): Boolean {
        return PrefUtils.lastUpdateTime + UPDATE_TIME_THRESHOLD < Calendar.getInstance().timeInMillis
    }

    private fun locationIsChanged(latitude: Double?, longitude: Double?): Boolean {
        val lastLocation = PrefUtils.lastLocation
        return if (lastLocation.first == -10000.0 || lastLocation.second == -10000.0)
            true
        else LocationUtils.distanceBetween(
            lastLocation.first,
            lastLocation.second,
            latitude ?: return false,
            longitude ?: return false
        ) > MAX_DISTANCE_TO_IGNORE_UPDATES
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    fun loadMore(offset: Int) = launch {
        val location = queryLiveData.value
            ?: if (PrefUtils.lastLocation.first == -10000.0 || PrefUtils.lastLocation.second == -10000.0) return@launch
            else "${PrefUtils.lastLocation.first},${PrefUtils.lastLocation.second}"
        isLoading = true
        repo.loadNextPage(offset, location)
        isLoading = false
    }
}