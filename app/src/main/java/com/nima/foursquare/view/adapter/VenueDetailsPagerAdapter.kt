package com.nima.foursquare.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class VenueDetailsPagerAdapter(fm: FragmentManager) :
    FragmentPagerAdapter(
        fm,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {
    private val _fragmentList = ArrayList<Fragment>()
    private val _fragmentTitleList = ArrayList<String>()

    override fun getItem(position: Int): Fragment = _fragmentList[position]

    override fun getCount(): Int = _fragmentList.size

    override fun getPageTitle(position: Int): CharSequence? = _fragmentTitleList[position]

    fun addFragment(fragment: Fragment, title: String) {
        _fragmentList.add(fragment)
        _fragmentTitleList.add(title)
    }
}