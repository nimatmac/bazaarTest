package com.nima.foursquare.view.fragment

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nima.foursquare.R
import com.nima.foursquare.view.viewmodel.VenueInfoViewModel
import com.nima.models.remote.vo.VenueViewObject
import com.nima.utils.UIUtils
import kotlinx.android.synthetic.main.venue_info_fragment.*


class VenueInfoFragment : Fragment() {


    private lateinit var viewModel: VenueInfoViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.venue_info_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        val venueId = arguments?.getString(KEY_VENUE_ID)!!
        viewModel = ViewModelProvider(this).get(VenueInfoViewModel::class.java)
        viewModel.venueId = venueId
        viewModel.getVenue().observe(viewLifecycleOwner, Observer { venue ->
            if (venue.venue?.hasDetails == false) {
                viewModel.loadVenueDetails()
            }
            bindData(venue)
            directionBtn.setOnClickListener {
                onLocationClicked(venue.venue?.latitude, venue.venue?.longitude)
            }
        })
    }

    private fun onLocationClicked(latitude: Double?, longitude: Double?) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("http://maps.google.com/maps?daddr=$latitude,$longitude")
        )
        startActivity(intent)
    }

    private fun bindData(venue: VenueViewObject) {
        address.text = venue.venue?.fullAddress
        category.text = venue.category?.name

        if (venue.venue?.hasDetails == true) {
            if (venue.venue?.priceMessage.isNullOrEmpty())
                priceLayout.visibility = View.GONE
            else
                priceLayout.visibility = View.VISIBLE

            price.text = venue.venue?.priceMessage

            rating.text = venue.venue?.rating.toString()

            val mDrawable = GradientDrawable()
            mDrawable.cornerRadius = UIUtils.pxFromDp(4f)
            mDrawable.setColor(Color.parseColor("#${venue.venue?.ratingColor}"))
            rating.background = mDrawable
        }
    }

    companion object {

        private const val KEY_VENUE_ID = "KEY_VENUE_ID"

        fun newInstance(venueId: String): VenueInfoFragment {
            val fragment = VenueInfoFragment()
            val args = Bundle()
            args.putString(KEY_VENUE_ID, venueId)
            fragment.arguments = args
            return fragment
        }
    }
}
