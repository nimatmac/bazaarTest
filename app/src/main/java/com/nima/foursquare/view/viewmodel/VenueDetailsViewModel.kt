package com.nima.foursquare.view.viewmodel

import androidx.lifecycle.ViewModel
import com.nima.repository.Repository

class VenueDetailsViewModel : ViewModel() {

    private val repo = Repository.getInstance()
    var venueId: String? = null

    fun loadVenue() = repo.getVenue(venueId)
}