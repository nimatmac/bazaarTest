package com.nima.foursquare.view.viewmodel

import androidx.lifecycle.ViewModel
import com.nima.repository.Repository

class VenueTipsViewModel : ViewModel() {

    private val repo = Repository.getInstance()

    fun getVenueTips(venueId: String) = repo.getVenueTips(venueId)
}
