package com.nima.foursquare.view.viewmodel

import androidx.lifecycle.ViewModel
import com.nima.repository.Repository

class VenuePhotosViewModel : ViewModel() {
    private val repo = Repository.getInstance()

    fun getVenuePhotos(venueId: String) = repo.getVenuePhotos(venueId)
}
