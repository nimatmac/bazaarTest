package com.nima.foursquare.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nima.foursquare.databinding.VenueTipListItemBinding
import com.nima.foursquare.view.viewholder.TipItemViewHolder
import com.nima.models.remote.entity.TipEntity

class VenueTipsAdapter : ListAdapter<TipEntity, RecyclerView.ViewHolder>(DIFF_CALLBACK) {
    
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        TipItemViewHolder(VenueTipListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as TipItemViewHolder).bind(getItem(position))
    }

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<TipEntity> =
            object : DiffUtil.ItemCallback<TipEntity>() {
                override fun areItemsTheSame(@NonNull oldItem: TipEntity, @NonNull newItem: TipEntity): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(@NonNull oldItem: TipEntity, @NonNull newItem: TipEntity): Boolean {
                    return oldItem.createdAt == newItem.createdAt &&
                            oldItem.text == newItem.text &&
                            oldItem.userPhoto == newItem.userPhoto &&
                            oldItem.venueId == newItem.venueId &&
                            oldItem.userName == newItem.userName &&
                            oldItem.agreeCount == newItem.agreeCount &&
                            oldItem.disagreeCount == newItem.disagreeCount
                }
            }
    }
}