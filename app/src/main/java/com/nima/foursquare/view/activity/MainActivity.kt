package com.nima.foursquare.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.nima.foursquare.R
import com.nima.foursquare.view.fragment.VenueDetailsFragment
import com.nima.foursquare.view.fragment.VenueListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setFragment(VenueListFragment(), false)
    }

    fun goToDetails(venueId: String) {
        val fragment = VenueDetailsFragment.newInstance(venueId)
        setFragment(fragment, true)
    }

    private fun setFragment(fragment: Fragment?, addToBackStack: Boolean) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()

        fragment?.let {
            fragmentTransaction.replace(R.id.fragmentLayout, it)
            if (addToBackStack)
                fragmentTransaction.addToBackStack(fragment.javaClass.simpleName)
        }
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        fragmentTransaction.commit()
        supportFragmentManager.executePendingTransactions()
    }
}
