package com.nima.foursquare.view.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.LocationServices
import com.nima.foursquare.ApplicationLoader
import com.nima.foursquare.R
import com.nima.foursquare.view.adapter.VenueListAdapter
import com.nima.foursquare.view.viewmodel.VenueListViewModel
import com.nima.utils.PermissionUtils
import kotlinx.android.synthetic.main.fragment_venue_list.*

class VenueListFragment : Fragment() {

    private lateinit var viewModel: VenueListViewModel
    private lateinit var adapter: VenueListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_venue_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        bindViewModel()
        checkLocationPermission(true)
    }

    private fun setupRecycler() {
        adapter = VenueListAdapter()
        recyclerView.adapter = adapter
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(
                recyclerView: RecyclerView, dx: Int,
                dy: Int
            ) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = recyclerView.layoutManager!!.childCount
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                val firstVisibleItemPosition: Int =
                    (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                if (!viewModel.isLoading) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount) {
                        viewModel.loadMore(totalItemCount)
                    }
                }
            }
        })
    }

    private fun checkLocationPermission(shouldRequest: Boolean) =
        when {
            PermissionUtils.hasLocationPermission(
                context ?: ApplicationLoader.instance.applicationContext
            ) -> {
                getLocation()
            }
            shouldRequest -> {
                PermissionUtils.getLocationPermission(this)
            }
            else -> {
                onPermissionDenied()
            }

        }

    private fun onPermissionDenied() {
        viewModel.getRecommendedVenues()
    }

    private fun getLocation() {
        LocationServices.getFusedLocationProviderClient(activity ?: return)
            .lastLocation.addOnCompleteListener { data ->
            viewModel.getRecommendedVenues(data.result?.latitude, data.result?.longitude)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PermissionUtils.REQUEST_PERMISSION_LOCATION) {
            checkLocationPermission(false)
        }
    }

    override fun onStop() {
        super.onStop()
        viewModel.recommendedVenues.removeObservers(viewLifecycleOwner)
    }

    private fun bindViewModel() {
        viewModel = ViewModelProvider(this).get(VenueListViewModel::class.java)
        viewModel.recommendedVenues.observe(viewLifecycleOwner, adapter::submitList)
    }
}
