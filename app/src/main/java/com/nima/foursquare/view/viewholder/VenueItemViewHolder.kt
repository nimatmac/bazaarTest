package com.nima.foursquare.view.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.nima.foursquare.databinding.VenueListItemBinding
import com.nima.foursquare.view.activity.MainActivity
import com.nima.models.remote.entity.VenueEntity
import com.nima.models.remote.vo.VenueViewObject

class VenueItemViewHolder(private val itemViewBinding: VenueListItemBinding) : RecyclerView.ViewHolder(itemViewBinding.root) {

    fun bind(item: VenueViewObject?) {
        itemViewBinding.venue = item
        itemViewBinding.callback = this
    }


    fun goToDetails(view: View, venueEntity: VenueEntity) {

        (view.context as MainActivity).goToDetails(venueEntity.venueId!!)
    }
}