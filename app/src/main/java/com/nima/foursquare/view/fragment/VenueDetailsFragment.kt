package com.nima.foursquare.view.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.nima.foursquare.R
import com.nima.foursquare.view.adapter.VenueDetailsPagerAdapter
import com.nima.foursquare.view.viewmodel.VenueDetailsViewModel
import kotlinx.android.synthetic.main.fragment_venue_details.*

class VenueDetailsFragment : Fragment() {

    private lateinit var viewModel: VenueDetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_venue_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }
        tabLayout.setupWithViewPager(viewPager)
        viewPager.adapter = VenueDetailsPagerAdapter(childFragmentManager).run {
            val venueId = arguments?.getString(KEY_VENUE_ID)!!
            addFragment(VenueInfoFragment.newInstance(venueId), "Info")
            addFragment(VenuePhotosFragment.newInstance(venueId), "Photos")
            addFragment(VenueTipsFragment.newInstance(venueId), "Tips")
            this
        }
    }

    private fun bindViewModel() {
        viewModel = ViewModelProvider(this).get(VenueDetailsViewModel::class.java)

        viewModel.venueId = arguments?.getString(KEY_VENUE_ID)

        viewModel.loadVenue().observe(viewLifecycleOwner, Observer { vo ->
            toolbar.title = vo.venue?.name
            if (vo.photo != null)
                Glide.with(context!!)
                    .load(vo.photo!!.getLargeImageUrl())
                    .into(collapsingImage)
        })

    }

    companion object {

        private const val KEY_VENUE_ID = "KEY_VENUE_ID"

        fun newInstance(venueId: String): VenueDetailsFragment {
            val fragment = VenueDetailsFragment()
            val args = Bundle()
            args.putString(KEY_VENUE_ID, venueId)
            fragment.arguments = args
            return fragment
        }
    }
}
