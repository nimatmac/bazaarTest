package com.nima.foursquare.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nima.foursquare.databinding.VenuePhotoListItemBinding
import com.nima.foursquare.view.viewholder.PhotoItemViewHolder
import com.nima.models.remote.entity.PhotoEntity

class VenuePhotosAdapter : ListAdapter<PhotoEntity, RecyclerView.ViewHolder>(DIFF_CALLBACK) {
    
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        PhotoItemViewHolder(VenuePhotoListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PhotoItemViewHolder).bind(getItem(position))
    }

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<PhotoEntity> =
            object : DiffUtil.ItemCallback<PhotoEntity>() {
                override fun areItemsTheSame(@NonNull oldItem: PhotoEntity, @NonNull newItem: PhotoEntity): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(@NonNull oldItem: PhotoEntity, @NonNull newItem: PhotoEntity): Boolean {
                    return oldItem.createdAt == newItem.createdAt &&
                            oldItem.prefix == newItem.prefix &&
                            oldItem.suffix == newItem.suffix &&
                            oldItem.venueId == newItem.venueId &&
                            oldItem.width == newItem.width &&
                            oldItem.height == newItem.height
                }
            }
    }
}