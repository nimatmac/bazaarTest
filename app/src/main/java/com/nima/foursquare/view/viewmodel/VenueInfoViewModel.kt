package com.nima.foursquare.view.viewmodel

import androidx.lifecycle.ViewModel
import com.nima.repository.Repository
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class VenueInfoViewModel : ViewModel(), CoroutineScope {
    private var repo = Repository.getInstance()
    lateinit var venueId: String
    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job + CoroutineExceptionHandler { _, t ->
            t.printStackTrace()
        }

    fun getVenue() = repo.getVenue(venueId)

    fun loadVenueDetails() = launch {
        repo.loadVenueDetails(venueId)
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}
