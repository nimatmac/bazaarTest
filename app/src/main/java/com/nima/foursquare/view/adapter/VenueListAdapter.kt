package com.nima.foursquare.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nima.foursquare.databinding.VenueListItemBinding
import com.nima.foursquare.view.viewholder.VenueItemViewHolder
import com.nima.models.remote.vo.VenueViewObject


class VenueListAdapter : ListAdapter<VenueViewObject, RecyclerView.ViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        VenueItemViewHolder(VenueListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VenueItemViewHolder).bind(getItem(position))
    }

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<VenueViewObject> =
            object : DiffUtil.ItemCallback<VenueViewObject>() {
                override fun areItemsTheSame(@NonNull oldItem: VenueViewObject, @NonNull newItem: VenueViewObject): Boolean {
                    return oldItem.venue?.venueId == newItem.venue?.venueId
                }

                override fun areContentsTheSame(@NonNull oldItem: VenueViewObject, @NonNull newItem: VenueViewObject): Boolean {
                    return oldItem.venue?.name == newItem.venue?.name &&
                    oldItem.venue?.fullAddress == newItem.venue?.fullAddress &&
                    oldItem.venue?.venueId == newItem.venue?.venueId &&
                    oldItem.venue?.longitude == newItem.venue?.latitude &&
                    oldItem.venue?.longitude == newItem.venue?.longitude
                }
            }
    }
}