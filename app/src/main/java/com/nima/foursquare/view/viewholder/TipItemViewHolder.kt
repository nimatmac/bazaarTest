package com.nima.foursquare.view.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.nima.foursquare.databinding.VenueTipListItemBinding
import com.nima.models.remote.entity.TipEntity

class TipItemViewHolder(private val itemViewBinding: VenueTipListItemBinding) : RecyclerView.ViewHolder(itemViewBinding.root) {

    fun bind(item: TipEntity?) {
        itemViewBinding.tip = item
    }
}