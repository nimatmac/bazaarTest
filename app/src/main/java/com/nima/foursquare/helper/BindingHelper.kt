package com.nima.foursquare.helper

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.nima.foursquare.R
import com.nima.models.remote.entity.VenueEntity
import java.text.DateFormat
import java.util.*

class BindingHelper {
    companion object {
        @JvmStatic
        @BindingAdapter("app:setImage")
        fun loadImage(view: ImageView, url: String?) {
            Glide.with(view.context)
                .load(url)
                .into(view)
        }

        @JvmStatic
        @BindingAdapter("app:setProfileImage")
        fun loadProfileImage(view: ImageView, url: String?) {
            Glide.with(view.context)
                .load(url)
                .placeholder(R.drawable.ic_user_placeholder)
                .into(view)
        }

        @JvmStatic
        @BindingAdapter("app:text")
        fun setText(view: TextView, venueEntity: VenueEntity) {
            val address =
                if (venueEntity.shortAddress != null) ", in " + venueEntity.shortAddress else ""
            view.text = "${venueEntity.distance} meters away$address"
        }

        @JvmStatic
        @BindingAdapter("app:date")
        fun setDate(view: TextView, timestamp: Long) {
            val date = Date(timestamp * 1000)
            val dateString = DateFormat.getDateInstance().format(date)
            view.text = dateString
        }
    }
}