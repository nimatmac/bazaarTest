package com.nima.foursquare

import android.app.Application
import com.bumptech.glide.Glide
import com.bumptech.glide.MemoryCategory
import com.nima.utils.ContextUtils

class ApplicationLoader: Application() {

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        ContextUtils.applicationContext = this
        Glide.get(this).setMemoryCategory(MemoryCategory.LOW)
    }

    companion object {
        lateinit var instance : ApplicationLoader
        private set
    }
}