package com.nima.repository

import androidx.lifecycle.LiveData
import com.nima.database.FoursquareDatabase
import com.nima.models.remote.entity.CategoryEntity
import com.nima.models.remote.entity.PhotoEntity
import com.nima.models.remote.entity.TipEntity
import com.nima.models.remote.entity.VenueEntity
import com.nima.models.remote.vo.VenueViewObject
import com.nima.remote.retrofit.RetrofitService
import com.nima.remote.webservice.WebserviceURLs
import com.nima.utils.NETWORK_PAGE_SIZE
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class Repository private constructor() {
    private val client = RetrofitService.createService(WebserviceURLs::class.java)
    private val db = FoursquareDatabase.getDatabase()
    private var isFinished = false


    fun getRecommendedVenues(): LiveData<List<VenueViewObject>> {
//        val factory: DataSource.Factory<Int, VenueEntity> = db.getVenuesPaged()
//
//        val pagedListBuilder: LivePagedListBuilder<Int, VenueEntity> =
//            LivePagedListBuilder(factory, NETWORK_PAGE_SIZE)
//        return pagedListBuilder
//            .setBoundaryCallback(BoundaryCallback(query, this))
//            .build()
        return db.venueDao().getVenues()
    }

    fun getVenuePhotos(venueId: String) = db.photoDao().getPhotosOfVenue(venueId).also {
        GlobalScope.launch(Dispatchers.IO + CoroutineExceptionHandler { _, t ->
            t.printStackTrace()
        }) {
            val count = getVenuePhotosCount(venueId)
            if (count == 0)
                loadVenuePhotos(venueId)
        }
    }

    fun getVenueTips(venueId: String) = db.tipDao().getTipsOfVenue(venueId).also {
        GlobalScope.launch(Dispatchers.IO + CoroutineExceptionHandler { _, t ->
            t.printStackTrace()
        }) {
            loadVenueTips(venueId)
        }
    }

    private suspend fun getVenuePhotosCount(venueId: String) =
        db.photoDao().getVenuePhotoCount(venueId)

    suspend fun loadVenueDetails(venueId: String) {
        val response = client.getVenueDetails(venueId)
        val item = response.data.venue
        val venue = db.venueDao().getVenue(venueId)
        venue ?: return
        venue.rating = item?.rating
        venue.ratingColor = item?.ratingColor
        venue.priceTier = item?.price?.tier
        venue.priceCurrency = item?.price?.currency
        venue.priceMessage = item?.price?.message
        venue.hasDetails = true
        db.venueDao().insert(venue)
    }

    private suspend fun loadVenuePhotos(venueId: String) {
        val response = client.getVenuePhotos(venueId)
        val items = response.data.photos.items.map { photo ->
            PhotoEntity(photo, venueId)
        }
        db.photoDao().insertAll(items)
    }

    private suspend fun loadVenueTips(venueId: String) {
        val response = client.getVenueTips(venueId)
        val items = response.data.tips.items.map { tip ->
            TipEntity(tip, venueId)
        }
        db.tipDao().insertAll(items)
    }

    suspend fun deleteAll() {
        db.venueDao().deleteAll()
        db.categoryDao().deleteAll()
        db.photoDao().deleteAll()
        db.tipDao().deleteAll()
    }

    fun getVenue(venueId: String?) = db.venueDao().getVenueLive(venueId)

    suspend fun loadNextPage(offset: Int, query: String) {
        if (isFinished)
            return
        val response = client.getRecommendedVenues(query, offset, NETWORK_PAGE_SIZE, 1)
        val items = response.data.groups.flatMap { group ->
            group.items.map { groupItem ->
                groupItem.venue
            }
        }.map { venue ->
            VenueEntity(venue)
        }
        if (items.size < NETWORK_PAGE_SIZE)
            isFinished = true
        db.venueDao().insertAll(items)

        val categories = response.data.groups.flatMap { group ->
            group.items.flatMap { groupItem ->
                groupItem.venue.categories.map { category ->
                    CategoryEntity(category)
                }
            }
        }
        db.categoryDao().insertAll(categories)
    }

    companion object {
        private var INSTANCE: Repository? = null
        fun getInstance(): Repository {
            if (INSTANCE == null) {
                synchronized(Repository::class) {
                    INSTANCE = Repository()
                }
            }
            return INSTANCE!!
        }
    }
}