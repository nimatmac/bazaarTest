package com.nima.remote.webservice

import com.nima.models.remote.response.ExploreResponse
import com.nima.models.remote.response.PhotosResponse
import com.nima.models.remote.response.TipsResponse
import com.nima.models.remote.response.VenueDetailResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface WebserviceURLs {
    @GET(WSConstants.EXPLORE_URL)
    suspend fun getRecommendedVenues(
        @Query(WSConstants.LATLONG) latLong: String,
        @Query(WSConstants.PAGE) page: Int,
        @Query(WSConstants.COUNT) count: Int,
        @Query(WSConstants.SORT) sortByDistance: Int
    ): ExploreResponse

    @GET(WSConstants.PHOTO_URL)
    suspend fun getVenuePhotos(@Path("id") venueId: String): PhotosResponse

    @GET(WSConstants.TIPS_URL)
    suspend fun getVenueTips(@Path("id") venueId: String): TipsResponse

    @GET(WSConstants.VENUE_DETAIL_URL)
    suspend fun getVenueDetails(@Path("id") venueId: String): VenueDetailResponse
}