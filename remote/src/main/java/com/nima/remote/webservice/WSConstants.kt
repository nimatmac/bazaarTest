package com.nima.remote.webservice

object WSConstants {
    //DEFAULT PARAMS
    const val CLIENT_ID = "client_id"
    const val CLIENT_SECRET = "client_secret"
    const val API_VERSION_DATE = "v"
    //URLS
    const val EXPLORE_URL = "venues/explore"
    const val PHOTO_URL = "venues/{id}/photos"
    const val TIPS_URL = "venues/{id}/tips"
    const val VENUE_DETAIL_URL = "venues/{id}"
    //PARAMS
    const val LATLONG = "ll"
    const val PAGE = "offset"
    const val COUNT = "limit"
    const val SORT = "sortByDistance"
}