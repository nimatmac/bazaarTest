package com.nima.remote.retrofit

import com.nima.remote.BuildConfig
import com.nima.remote.webservice.WSConstants
import com.nima.utils.ContextUtils
import com.nima.utils.NetworkUtils.isConnected
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
    private const val cacheSize = (5 * 1024 * 1024).toLong()
    private val myCache = Cache(ContextUtils.applicationContext.cacheDir, cacheSize)
    private val logging =
        HttpLoggingInterceptor().setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)
    private val client = OkHttpClient.Builder()
        .cache(myCache)
        .addInterceptor(logging)
        .addInterceptor { chain ->
            val url = chain.request().url().newBuilder()
                .addQueryParameter(WSConstants.CLIENT_ID, BuildConfig.FOURSQUARE_CLIENT_ID)
                .addQueryParameter(WSConstants.CLIENT_SECRET, BuildConfig.FOURSQUARE_CLIENT_SECRET)
                .addQueryParameter(
                    WSConstants.API_VERSION_DATE,
                    BuildConfig.FOURSQUARE_API_VERSION_DATE
                )
                .build()
            val request = chain.request().newBuilder()
                .url(url).build()
            chain.proceed(request)
        }
        .addNetworkInterceptor { chain ->
            val original = chain.proceed(chain.request())
            if (isConnected()) {
                original.newBuilder()
                    .header("Cache-Control", "public, max-age=" + 60 * 60 * 2)// 2 hours if online
                    .build()
            } else {
                original.newBuilder()
                    .header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7) // 7 days if offline
                    .build()
            }
        }
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.SERVER_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    fun <S> createService(serviceClass: Class<S>): S {
        return retrofit.create(serviceClass)
    }
}