package com.nima.models.remote.response

data class Venue(
    val id: String,
    val name: String?,
    val location: Location?,
    val categories: List<Category>,
    val rating : Float?,
    val ratingColor: String?,
    val price: Price?)