package com.nima.models.remote.response

class Tip(
    val id: String,
    val createdAt: Long?,
    val text: String?,
    val user: User?,
    val agreeCount: Int?,
    val disagreeCount: Int?
)