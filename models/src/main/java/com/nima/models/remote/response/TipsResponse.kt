package com.nima.models.remote.response

import com.google.gson.annotations.SerializedName

data class TipsResponse(@SerializedName("meta") val metaData: Meta,
                        @SerializedName("response") val data: TipsResponseData)