package com.nima.models.remote.response

data class ResponseData(
    val headerFullLocation: String?,
    val totalResults: Int?,
    val groups: List<Group>
)