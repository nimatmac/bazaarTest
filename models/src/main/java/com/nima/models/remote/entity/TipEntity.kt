package com.nima.models.remote.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nima.models.remote.TIP_TABLE_NAME
import com.nima.models.remote.response.Tip

@Entity(tableName = TIP_TABLE_NAME)
class TipEntity() {
    @PrimaryKey
    @ColumnInfo(name = "${TIP_TABLE_NAME}_id")
    var id: String = ""
    @ColumnInfo(name = "${TIP_TABLE_NAME}_createdAt")
    var createdAt: Long? = null
    @ColumnInfo(name = "${TIP_TABLE_NAME}_agreeCount")
    var agreeCount: Int? = null
    @ColumnInfo(name = "${TIP_TABLE_NAME}_disagreeCount")
    var disagreeCount: Int? = null
    @ColumnInfo(name = "${TIP_TABLE_NAME}_text")
    var text: String? = null
    @ColumnInfo(name = "${TIP_TABLE_NAME}_userName")
    var userName: String? = null
    @ColumnInfo(name = "${TIP_TABLE_NAME}_userPhoto")
    var userPhoto: String? = null
    @ColumnInfo(name = "${TIP_TABLE_NAME}_venueId")
    var venueId: String? = null

    constructor(tip: Tip, venueId: String) : this() {
        this.id = tip.id
        this.createdAt = tip.createdAt
        this.agreeCount = tip.agreeCount
        this.disagreeCount = tip.disagreeCount
        this.text = tip.text
        this.userName = "${tip.user?.firstName} ${tip.user?.lastName}"
        this.userPhoto = "${tip.user?.photo?.prefix}88${tip.user?.photo?.suffix}"
        this.venueId = venueId
    }
}