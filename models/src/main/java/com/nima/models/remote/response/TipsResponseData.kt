package com.nima.models.remote.response

data class TipsResponseData(
    val tips: Tips
)