package com.nima.models.remote.vo

import androidx.room.Embedded
import com.nima.models.remote.entity.CategoryEntity
import com.nima.models.remote.entity.PhotoEntity
import com.nima.models.remote.entity.VenueEntity

class VenueViewObject {
    @Embedded
    var venue: VenueEntity? = null
    @Embedded
    var category: CategoryEntity? = null
    @Embedded
    var photo: PhotoEntity? = null
}