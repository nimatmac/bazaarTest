package com.nima.models.remote.response

import com.google.gson.annotations.SerializedName

data class Icon(@SerializedName("prefix") val prefix: String?,
                @SerializedName("suffix") val suffix: String?) {
    override fun toString(): String {
        return "${prefix}88$suffix"
    }
}