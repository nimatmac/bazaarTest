package com.nima.models.remote.response

data class User(
    val id: String,
    val firstName: String,
    val lastName: String,
    val photo: Photo
)