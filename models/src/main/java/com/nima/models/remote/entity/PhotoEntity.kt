package com.nima.models.remote.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nima.models.remote.PHOTO_TABLE_NAME
import com.nima.models.remote.response.Photo

@Entity(tableName = PHOTO_TABLE_NAME)
class PhotoEntity() {
    @PrimaryKey
    @ColumnInfo(name = "${PHOTO_TABLE_NAME}_id")
    var id: String = ""
    @ColumnInfo(name = "${PHOTO_TABLE_NAME}_createdAt")
    var createdAt: Long? = null
    @ColumnInfo(name = "${PHOTO_TABLE_NAME}_prefix")
    var prefix: String? = null
    @ColumnInfo(name = "${PHOTO_TABLE_NAME}_suffix")
    var suffix: String? = null
    @ColumnInfo(name = "${PHOTO_TABLE_NAME}_width")
    var width: Int? = null
    @ColumnInfo(name = "${PHOTO_TABLE_NAME}_height")
    var height: Int? = null
    @ColumnInfo(name = "${PHOTO_TABLE_NAME}_venueId")
    var venueId: String? = null

    constructor(photo: Photo, venueId: String) : this() {
        this.id = photo.id
        this.createdAt = photo.createdAt
        this.prefix = photo.prefix
        this.suffix = photo.suffix
        this.width = photo.width
        this.height = photo.height
        this.venueId = venueId
    }

    fun getNormalImageUrl() = "${prefix}300x300$suffix"
    fun getLargeImageUrl() = "${prefix}500x500$suffix"
    fun getOriginalImageUrl() = "${prefix}original$suffix"
}