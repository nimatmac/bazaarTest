package com.nima.models.remote.response

data class Price(
    val tier: Int?,
    val message: String,
    val currency: String
)