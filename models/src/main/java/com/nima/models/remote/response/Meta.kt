package com.nima.models.remote.response

import com.google.gson.annotations.SerializedName

data class Meta(@SerializedName("code") val statusCode: Int,
                @SerializedName("requestId") val reqId: String)