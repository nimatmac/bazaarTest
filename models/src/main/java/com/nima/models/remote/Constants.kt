package com.nima.models.remote

const val VENUE_TABLE_NAME = "venue"
const val CATEGORY_TABLE_NAME = "category"
const val PHOTO_TABLE_NAME = "photo"
const val TIP_TABLE_NAME = "tip"
