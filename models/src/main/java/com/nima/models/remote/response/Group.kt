package com.nima.models.remote.response

data class Group(
    val type: String,
    val name: String,
    val items: List<GroupItem>
)