package com.nima.models.remote.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nima.models.remote.VENUE_TABLE_NAME
import com.nima.models.remote.response.Venue

@Entity(tableName = VENUE_TABLE_NAME)
class VenueEntity() {
    @PrimaryKey
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_venueId")
    var venueId: String = ""
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_name")
    var name: String? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_fullAddress")
    var fullAddress: String? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_address")
    var shortAddress: String? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_country")
    var country: String? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_city")
    var city: String? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_distance")
    var distance: Int? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_lat")
    var latitude: Double? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_lng")
    var longitude: Double? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_catId")
    var categoryId: String? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_hasDetails")
    var hasDetails: Boolean = false
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_rating")
    var rating: Float? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_ratingColor")
    var ratingColor: String? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_priceTier")
    var priceTier: Int? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_priceMessage")
    var priceMessage: String? = null
    @ColumnInfo(name = "${VENUE_TABLE_NAME}_priceCurrency")
    var priceCurrency: String? = null

    constructor(venue: Venue) : this() {
        venueId = venue.id
        name = venue.name
        fullAddress = venue.location?.formattedAddress?.joinToString { it ->
            it
        }
        shortAddress = venue.location?.address
        country = venue.location?.country
        city = venue.location?.city
        distance = venue.location?.distance
        latitude = venue.location?.lat
        longitude = venue.location?.lng
        categoryId = venue.categories.firstOrNull()?.id
    }
}