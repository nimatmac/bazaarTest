package com.nima.models.remote.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nima.models.remote.CATEGORY_TABLE_NAME
import com.nima.models.remote.response.Category
@Entity(tableName = CATEGORY_TABLE_NAME)
class CategoryEntity() {
    @PrimaryKey
    @ColumnInfo(name = "${CATEGORY_TABLE_NAME}_categoryId")
    var categoryId: String = ""
    @ColumnInfo(name = "${CATEGORY_TABLE_NAME}_name")
    var name: String? = null
    @ColumnInfo(name = "${CATEGORY_TABLE_NAME}_shortName")
    var shortName: String? = null
    @ColumnInfo(name = "${CATEGORY_TABLE_NAME}_icon")
    var icon: String? = null
    constructor(category: Category) : this() {
        categoryId = category.id!!
        name = category.name
        shortName = category.shortName
        icon = category.icon.toString()
    }
}