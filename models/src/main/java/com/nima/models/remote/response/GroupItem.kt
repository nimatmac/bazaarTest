package com.nima.models.remote.response

import com.google.gson.annotations.SerializedName

data class GroupItem(@SerializedName("venue") val venue: Venue)