package com.nima.models.remote.response

data class VenueDetailResponseData(
    val venue: Venue?
)