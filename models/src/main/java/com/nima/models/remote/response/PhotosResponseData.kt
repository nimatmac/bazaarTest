package com.nima.models.remote.response

data class PhotosResponseData(
    val photos: Photos
)