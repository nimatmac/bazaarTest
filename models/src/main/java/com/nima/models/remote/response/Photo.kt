package com.nima.models.remote.response

data class Photo(
    val id: String,
    val createdAt: Long?,
    val prefix: String?,
    val suffix: String?,
    val width: Int?,
    val height: Int?
)