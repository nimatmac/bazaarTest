package com.nima.models.remote.response

data class Tips(
    val count: Int?,
    val items: List<Tip>)