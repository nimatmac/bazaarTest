package com.nima.models.remote.response

data class Location(
    val address: String?,
    val lat: Double?,
    val lng: Double?,
    val distance: Int?,
    val city: String?,
    val state: String?,
    val country: String?,
    val formattedAddress: List<String>
)