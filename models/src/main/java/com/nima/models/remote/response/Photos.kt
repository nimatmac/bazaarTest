package com.nima.models.remote.response

data class Photos(
    val count: Int?,
    val items: List<Photo>)